// import packages
import React, { Component } from 'react';
import ReactDOM from "react-dom";
import AddToDo from './components/add_todo';
import ListToDo from './components/list_todo';


// Create a component
class App extends Component {
    // setup the state
    constructor(props) {
        super(props);
        
        // app boots up. both are set to nothing
        this.state = { 
            toDo: []
        };
        this.addToDo = this.addToDo.bind(this);
        this.removeToDo = this.removeToDo.bind(this);
    }

    addToDo(item) {
        console.log("Item to be added: " +item)
        var newList = this.state.toDo;
        newList.push(item);
        this.setState({
            toDo: newList
        });
    }

    removeToDo(item){
        console.log("Item being removed: " +item)
        var newList = this.state.toDo;
        var i = newList.indexOf(item)

        newList.splice(i, 1);
        this.setState({
            toDo: newList
        });
    }


    render() {
        return (
            <div>
                <AddToDo onToDoAdd={this.addToDo} />
                <ListToDo listToDo={this.state.toDo}  removeToDo={this.removeToDo} />
            </div>
        );
    };
}

export default App;
