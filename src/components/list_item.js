import React, { Component } from 'react';

const ListToDoItem = (item) => {

    return (
        <div class="list-hover list-group-item">
            <div class="row">
                <div class="col-9">
                    <p>{item.toDo}</p>
                </div>
                <div class="col">
                    <button onClick={() => { item.delete(item.toDo)}} type="button" className="btn btn-primary btn-sm align-left">Delete</button>
                </div>
            </div>
        </div>
    );
}

export default ListToDoItem;