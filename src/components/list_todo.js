import React, { Component } from 'react';
import ListToDoItem from './list_item';


    


  const ListToDo = props => {
    const listItem = props.listToDo.map((list, index) => {
        return (
            <ListToDoItem
                key={index}
                toDo={list}
                delete={props.removeToDo}
            />
        );
    });


    return (
        <ul className="list-group pt-4">
            {listItem}
        </ul>
    );
  }

export default ListToDo;
